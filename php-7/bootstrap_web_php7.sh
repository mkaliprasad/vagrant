#!/usr/bin/env bash

hostname xola-web-php7
echo "xola-web-php7" > /etc/hostname
sudo su
apt update
apt upgrade -y

# PHP
apt install -y php7.2 php7.2-cli php7.2-fpm
apt install -y php-apcu php7.2-curl php7.2-gd php-imagick php7.2-intl php7.2-json php7.2-mbstring php7.2-xml php7.2-zip

# Mcrypt for PHP
apt install -y libmcrypt-dev php-pear php7.2-dev
pecl install mcrypt-1.0.1
echo "extension=mcrypt.so" > /etc/php/7.2/mods-available/mcrypt.ini
phpenmod mcrypt

# MongoDB driver for PHP
pecl install mongodb
echo "extension=mongodb.so" > /etc/php/7.2/mods-available/mongodb.ini
phpenmod mongodb


# Composer for PHP
apt install -y composer npm
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs

# Node, NPM for LESS
sudo apt -y install nodejs npm
sudo npm install -g less@1.3.3
sudo npm install -g handlebars@1.3
sudo npm install -g grunt

# PhantomJS for Jasmine
sudo npm install -g phantomjs

# Ant
sudo apt install -y openjdk-8-jdk-headless ant

# Nginx
sudo sudo apt-get install nginx-common
sudo sudo apt-get install nginx
sudo apt install -y nginx-extras

# ACL for Symfony
sudo apt install -y acl

# Configure github ssh

mkdir -p /home/vagrant/.ssh
cp /vagrant/config/ssh/* ~/.ssh/
sudo chmod 600 ~/.ssh/id_rsa_github

# GeoIP for PHP
sudo su
apt install -y libmaxminddb0 libmaxminddb-dev mmdb-bin
git clone --depth 1 https://github.com/maxmind/MaxMind-DB-Reader-php.git
cd MaxMind-DB-Reader-php/ext
phpize
./configure
make
make install
cd
echo "extension=maxminddb.so" > /etc/php/7.2/mods-available/maxminddb.ini
phpenmod maxminddb
exit

# Install xola
echo "127.0.0.1 xola.local" >> /etc/hosts
cd /var/www
git clone git@github.com:xola/xola.git --depth 1
cd xola
sudo mkdir -p app/cache app/spool web/uploads web/cache app/logs
sudo chmod 777 app/cache app/spool web/uploads web/cache app/logs
sudo chown -R vagrant:www-data .
sudo setfacl -R -m u:www-data:rwX -m u:vagrant:rwX app/cache app/logs app/spool
sudo setfacl -dR -m u:www-data:rwX -m u:vagrant:rwX app/cache app/logs app/spool


# Configure nginx

sudo mkdir -p /data/nginx/cache
sudo chown www-data:www-data /data/nginx/cache
sudo cp -r /vagrant/config/nginx/ssl /etc/nginx/ssl
sudo mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
sudo cp /vagrant/config/nginx/nginx.conf /etc/nginx/nginx.conf
sudo cp /vagrant/config/nginx/sites-available/* /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/xola /etc/nginx/sites-enabled/xola

systemctl restart nginx.service

# Configure FPM 
cp /vagrant/config/fpm/xola.conf /etc/php/7.2/fpm/pool.d/xola.conf
cp /vagrant/config/password.test.yml /var/www/xola/app/config/
service php7.2-fpm restart
