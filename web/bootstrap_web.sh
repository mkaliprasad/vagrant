#!/bin/bash

hostname xola-web
echo "xola-web" > /etc/hostname

aptitude update

# Install ntp to synchronize clocks https://help.ubuntu.com/10.04/serverguide/NTP.html
aptitude -y install ntp

# Install LESS and its dependencies
aptitude -y install nodejs npm
npm install -g less@1.3.3

# Install PHP and necessary PHP modules
add-apt-repository -y ppa:ondrej/php
aptitude update
aptitude install -y php5.6 php5.6-cgi php5.6-cli php5.6-curl php5.6-dom php5.6-gd php5.6-imagick php5.6-intl php5.6-json php5.6-mbstring php5.6-mcrypt php5.6-mongo php5.6-xml php5.6-zip

# Run in all environments EXCEPT unit test slaves
aptitude install -y php5.6-fpm
cp /vagrant/config/fpm/xola.conf /etc/php/5.6/fpm/pool.d/xola.conf
rm -f /etc/php/5.6/fpm/pool.d/www.conf
apt-get install --no-install-recommends php5.6-apcu
service php5.6-fpm restart

# Run in DEV ONLY
aptitude install -y php5.6-dev php5.6-xdebug

# Install nginx
aptitude -y install nginx nginx-extras 

# Install DVCS
aptitude -y install git libcurl4-openssl-dev pkg-config

# Install Java
aptitude -y install default-jre-headless

# Install ACL (for Symfony)
aptitude -y install acl

# Install composer 
apt-get install curl
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

# Configure github ssh
cp /vagrant/config/ssh/* ~/.ssh/
chmod 600 ~/.ssh/id_rsa_github
mkdir -p /var/www

# Install xola
echo "127.0.0.1 xola.local xola.dev" >> /etc/hosts
cd /var/www
git clone git@github.com:xola/xola.git --depth 1
cd xola
mkdir -p app/cache app/spool app/logs web/uploads web/cache 
chmod 777 app/cache app/spool app/logs web/uploads web/cache
chown -R vagrant:www-data .
setfacl -R -m u:www-data:rwX -m u:vagrant:rwX app/cache app/logs app/spool
setfacl -dR -m u:www-data:rwX -m u:vagrant:rwX app/cache app/logs app/spool

# Install xola static
cd /var/www
git clone git@github.com:xola/xola-static.git --depth 1
cd xola-static
mkdir -p app/cache app/spool web/uploads web/cache
chmod 777 app/cache app/spool web/uploads web/cache
chown -R vagrant:www-data .
setfacl -R -m u:www-data:rwX -m u:vagrant:rwX app/cache app/logs app/spool
setfacl -dR -m u:www-data:rwX -m u:vagrant:rwX app/cache app/logs app/spool

# Configure nginx
mkdir -p /data/nginx/cache
chown www-data:www-data /data/nginx/cache
cp -r /vagrant/config/nginx/ssl /etc/nginx/ssl
mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
cp /vagrant/config/nginx/nginx.conf /etc/nginx/nginx.conf
cp /vagrant/config/nginx/sites-available/* /etc/nginx/sites-available/
ln -s /etc/nginx/sites-available/xola /etc/nginx/sites-enabled/xola
ln -s /etc/nginx/sites-available/static /etc/nginx/sites-enabled/static
ln -s /etc/nginx/sites-available/localhost /etc/nginx/sites-enabled/localhost
rm /etc/nginx/sites-enabled/default
service nginx start

# Configure PHP-FPM
cp /vagrant/config/fpm/xola.conf /etc/php/5.6/fpm/pool.d/xola.conf
service php5.6-fpm restart
phpenmod mcrypt

#Setup the Web box
cd /var/www/xola
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
apt-get install -y nodejs
npm install
cd /var/www/xola/app/config
cp /vagrant/config/symfony/password.dev.yml .
cd ../../
HTTPDUSER=`ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs
setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs
composer install
service nginx restart
./cc --nodump
app/console fos:user:create $username=admin@xola.local $email=admin@xola.local $password=Xola123
app/console fos:user:promote $username=admin@xola.local $role=ROLE_ADMIN
./cc --nodump
